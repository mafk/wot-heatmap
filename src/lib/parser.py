# -*- coding: utf-8 -*-
__author__ = 'mmm'


import zlib, re, json, struct, sys, os, os.path
from Crypto.Cipher import Blowfish
import hashlib
import settings
from packets import factory


headers_explanation = ("arena_information", "battle_statistics", "aftermatch_statistics")


class Error(Exception):
    pass


class Headers():
    def __init__(self, replay_data):
        self.headers_dict = {}
        self.replay_data = replay_data
        self.headers_raw = ''
        data = replay_data
        data = data[4:]
        headers_number = struct.unpack('=i', data[:4])[0]
        data = data[4:]

        for iHeader in xrange(headers_number):
            header_len = struct.unpack('=i', data[:4])[0]
            data = data[4:]

            header_str = data[:header_len]
            self.headers_raw += header_str

            try:
                header = json.loads(header_str)
            except ValueError:
                raise Error('Failed to read json header number ' + str(iHeader))

            self.headers_dict[headers_explanation[iHeader]] = header
            data = data[header_len:]

    def get_header_dict(self):
        return self.headers_dict

    def get_headers_raw(self):
        return self.headers_raw


class Body():
    def __init__(self, replay_data):
        self.body = self._body(replay_data)
        self.unzipped_size = struct.unpack('=i', self.body[0:4])[0]
        self.zipped_size = struct.unpack('=i', self.body[4:8])[0]
        self.encoded = self.body[8:]
        self.decoded = self._decode_blowfish_data(self.encoded)
        self.unzipped = self._unzip()

    def _unzip(self):
        try:
            unzipped = zlib.decompress(self.decoded[0:self.zipped_size])
        except BaseException as e:
            raise Error('Failed to unzip: %s' % e.message )
        if len(unzipped) != self.unzipped_size:
            raise Error('Unzipped size is wrong.')
        return unzipped

    def _body(self, replay_data):
        data = replay_data
        data = data[4:]
        headers_number = struct.unpack('=i', data[:4])[0]
        data = data[4:]

        for iHeader in xrange(headers_number):
            header_len = struct.unpack('=i', data[:4])[0]
            data = data[4:]
            data = data[header_len:]

        return data

    def _xor_crypt_string(self, s1, s2):
        return ''.join(chr(ord(a) ^ ord(b)) for a, b in zip(s1, s2))

    def _decode_blowfish_data(self, s):
        decoded = ""
        prev_block = None
        blocks_number = len(s) / 8
        bfc = Blowfish.new(settings.SECURE_KEY)

        for i in range(blocks_number):
            f = 0 + (8*i)
            t = f + 8
            tmp = bfc.decrypt(s[f:t])
            if prev_block is not None:
                tmp = self._xor_crypt_string(tmp, prev_block)
            prev_block = tmp
            decoded += tmp
        return decoded


class Parser():
    def __init__(self, filename=None):
        if filename is not None:
            self.filename = filename
        if not os.path.isfile(self.filename):
            raise Error("No such file: %s" % self.filename)
        self.replay_data = self._read_file()
        self.headers = Headers(self.replay_data)
        self.body = Body(self.replay_data)
        self.packets = self._packets()
        self._validate()

    def _validate(self):
        md5_headers = hashlib.md5(self.headers.headers_raw).digest()
        md5_encrypted_list = [p for p in self.filter(lambda x: x.id == -1)]
        md5_encrypted = md5_encrypted_list[0]._payload
        if md5_headers != md5_encrypted:
            raise Error("Headers modified!")

    def _read_file(self):
        if self.filename is None:
            raise Error("Filename not specified.")
        try:
            f = open(self.filename, 'rb')
        except IOError as e:
            raise Error(str(e))
        s = f.read()
        f.close()
        return s

    def filter(self, condition):
        for p in self.packets:
            if condition(p):
                yield p

    def _packets(self):
        """
        :return: list of packages
        """
        data_length = len(self.body.unzipped)
        cur = 0
        packets = []
        while cur < data_length:
            l = struct.unpack('=i', self.body.unzipped[cur:cur+4])[0]   # length in bytes
            packet = factory(self.body.unzipped[cur:cur+12+l])
            packets.append(packet)
            cur += 12 + l  # header is always 12 bytes
        return packets