# -*- coding: utf-8 -*-
__author__ = 'mmm'

import struct
import json
import math


def float_to_round_string(float, scale):
    return ('%.' + str(scale) + 'f') % round(float, scale)


class BasePacket(object):
    def __init__(self, data):
        self._raw_data = data
        self._len = struct.unpack('=i', self._raw_data[0:4])[0]
        self.id = struct.unpack('=i', self._raw_data[4:8])[0]
        self.ts = struct.unpack('=f', self._raw_data[8:12])[0]
        self.ts = float_to_round_string(self.ts, 1)
        self._payload = self._raw_data[12:12+self._len]
        self._payload_hex = " ".join("{:02x}".format(ord(c)) for c in self._payload)

    def __str__(self):
        return "{cl}, id: {id}, ts: {ts}, len: {len}, payload: {payload}".\
            format(cl=self.__class__.__name__, id=self.id, ts=self.ts, len=self._len, payload=self._payload)

    def to_JSON(self):
        my_dict = self.__dict__
        my_dict['purpose'] = self.__class__.__name__
        for k in my_dict.keys():
            if k[0:1] == '_':
                my_dict.pop(k, None)
        return json.dumps(my_dict)
        # return json.dumps(self, default=lambda o: o.__dict__,
        #     sort_keys=True, indent=4)
    # def purpose(self):
    #     return self.__class__.__name__


class GameUpdates(BasePacket):
    def __init__(self, data):
        super(GameUpdates, self).__init__(data)
        self.player_id = struct.unpack('=i', self._payload[0:4])[0]
        self.player_id_hex = " ".join("{:02x}".format(ord(c)) for c in self._payload[0:4])
        self.subtype = struct.unpack('=i', self._payload[4:8])[0]
        self._data_length = struct.unpack('=i', self._payload[8:12])[0]
        self._data = self._payload[12:12+self._data_length]
        #self.payload_hex = self._payload_hex
        if self.subtype == 0x00:  # shot
            self.shot = 1
            self.xz = self._payload_hex
        if self.subtype == 0x07:  # hit
            self.source = struct.unpack('=i', self._data[0:4])[0]
        if self.subtype == 0x01:  # new health
            self.source = struct.unpack('=i', self._data[2:6])[0]
            self.health = max(0, struct.unpack('=h', self._data[0:2])[0])
            self.xz = self._payload_hex
        if self.subtype == 40:  # reconnect?
            self.xz = self._payload_hex
            sub_subtype = struct.unpack('=i', self._payload[8:12])[0]
            if sub_subtype == 10:
                self.xx = struct.unpack('=i', self._payload[17:21])[0]
            #self.xx = struct.unpack('=i', self._data[23:27])[0]



class Version(BasePacket):
    def __init__(self, data):
        super(Version, self).__init__(data)
        self.version_len = struct.unpack('=i', self._payload[0:4])[0]
        self.version = self._payload[4:4+self.version_len]


class GameStatus(BasePacket):
    def __init__(self, data):
        super(GameStatus, self).__init__(data)
        self.status = struct.unpack('=i', self._payload[0:4])[0]


class TurretAngle(BasePacket):
    def __init__(self, data):
        super(TurretAngle, self).__init__(data)
        self.player_id = struct.unpack('=i', self._payload[0:4])[0]
        self.subtype = struct.unpack('=i', self._payload[4:8])[0]
        self._subtype_len = struct.unpack('=i', self._payload[8:12])[0]
        if self.subtype == 0x00:  # x3
            pass
        if self.subtype == 0x02:  # turret angle
            self._subtype_data = struct.unpack('=H', self._payload[12:14])[0]
            self.turret_angle = float_to_round_string(math.pi * float(self._subtype_data - 32768) / float(32768), 2)
        if self.subtype == 0x04:  # x3
            pass


class Position(BasePacket):
    def __init__(self, data):
        super(Position, self).__init__(data)
        self.player_id = struct.unpack('=i', self._payload[0:4])[0]
        self.x = float_to_round_string(struct.unpack('=f', self._payload[12:16])[0], 2)
        self.z = float_to_round_string(struct.unpack('=f', self._payload[16:20])[0], 2)
        self.y = float_to_round_string(struct.unpack('=f', self._payload[20:24])[0], 2)
        self.yaw = float_to_round_string(struct.unpack('=f', self._payload[36:40])[0], 2)
        self.pitch = float_to_round_string(struct.unpack('=f', self._payload[40:44])[0], 2)
        self.roll = float_to_round_string(struct.unpack('=f', self._payload[44:48])[0], 2)
        self.player_id_hex = " ".join("{:02x}".format(ord(c)) for c in self._payload[0:4])
        self.payload_hex = self._payload_hex

    def __str__(self):
        return "{cl}, id: {id}, ts: {ts}, player_id: {player_id}  ".\
            format(cl=self.__class__.__name__, id=self.id, ts=self.ts, player_id=self.player_id) \
            + "coords(x,y,z): ({x}, {y}, {z}) ".format(x=self.x, y=self.y, z=self.z) \
            + "hull_orientation(yaw, pitch, roll): ({yaw}, {pitch}, {roll})".format(
                yaw=self.yaw, pitch=self.pitch, roll=self.roll)


class Chat(BasePacket):
    def __init__(self, data):
        super(Chat, self).__init__(data)
        self.len = struct.unpack('=i', self._payload[0:4])[0]
        self.chat = self._payload[4:4+self.len]


class PlayerId(BasePacket):
    def __init__(self, data):
        super(PlayerId, self).__init__(data)
        self.player_id = struct.unpack('=i', self._payload[0:4])[0]


class Checksum(BasePacket):
    pass


class Packet5(BasePacket):
    def __init__(self, data):
        super(Packet5, self).__init__(data)
        self.player_id = struct.unpack('=i', self._payload[0:4])[0]
        #self.payload = self._payload
        self.payload_hex = self._payload_hex


packet_types = {
      -1: Checksum,
    0x08: GameUpdates,
    0x0A: Position,
    0x12: GameStatus,
    0x14: Version,
    0x1F: Chat,
    0x1E: PlayerId,
    0x07: TurretAngle,
    0x05: Packet5,
}


def factory(data):
    type = struct.unpack('=i', data[4:8])[0]
    return packet_types.get(type, BasePacket)(data)
