# -*- coding: utf-8 -*-
__author__ = 'mmm'

from lib.parser import Parser
import sys
from json import JSONEncoder

if len(sys.argv) > 1:
    parser = Parser(sys.argv[1])
else:
    parser = Parser('/home/mmm/Downloads/14456065847580_ussr_R107_LTB_lakeville.wotreplay')


def float_to_round_string(float, scale):
    return ('%.' + str(scale) + 'f') % round(float, scale)


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


def get_damage_filter(pid):
    return lambda x: ((x.id == 7 and x.subtype == 2) or x.id == 8) and x.player_id == pid


def get_packet_5():
    return lambda x: x.id == 5


def get_positions(pid):
    return lambda x: (x.id == 10 and pid == x.player_id)


player_name = parser.headers.get_header_dict()['arena_information']['playerName']
my_team_number = None

for v in parser.headers.get_header_dict()['arena_information']['vehicles'].values():
    if v['name'] == player_name:
        my_team_number = v['team']

my_team = []
enemy_team = []

for k,v in parser.headers.get_header_dict()['arena_information']['vehicles'].items():
    if v['team'] == my_team_number:
        my_team.append(k)
    else:
        enemy_team.append(k)

enemy_team_movements = {}

for pid in enemy_team:
    positions = [p for p in parser.filter(get_positions(int(pid)))]
    x, y, z = 0, 0, 0
    if positions:
        x = max([float(p.x) for p in positions]) - min([float(p.x) for p in positions])
        y = max([float(p.y) for p in positions]) - min([float(p.y) for p in positions])
        z = max([float(p.z) for p in positions]) - min([float(p.z) for p in positions])
    total_movement = float(float_to_round_string(max(x,y,z), 2))
    enemy_team_movements[pid] = total_movement

afk, total = 0, 0

for k, v in enemy_team_movements.items():
    if v < 1:
        afk += 1
    total += 1

print "Enemy team afkers: " + float_to_round_string((100 * float(afk) / total), 2) + "%"

