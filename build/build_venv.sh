#!/bin/bash

CURR_DIR=`pwd`
cd `dirname $0`
SCRIPT_DIR=`pwd`

if [ "$CURR_DIR" != "$SCRIPT_DIR" ]; then
    echo "This script must be running from $SCRIPT_DIR" >&2
    exit 1
fi


rm -rf ../venv
virtualenv -p /usr/bin/python --no-site-packages ../venv
source ../venv/bin/activate
pip install -r requirements.txt

